﻿using System;
using System.Windows.Forms;

namespace DiceGuessGame_GUI
{
    static class Program
    {
        
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new DiceGame());
        }
    }
}
