﻿using System;
using System.Windows.Forms;

namespace DiceGuessGame_GUI
{
    public partial class DiceGame : Form
    {
        private Random rnd;
        private int roll, d1, d2;

        // Constructor sets the selected index of the choice box to first one.
        // Then it displays a greeting message in the text box.
        public DiceGame()
        {
            InitializeComponent();
            rnd = new Random();
            guessChoiceBox.SelectedIndex = 0;
            textBox.AppendText("Welcome to Henri's dice guessing game.\n");
            textBox.AppendText("Pick a number and click Roll to play.\n");
        }

        private void quitButton_Click(object sender, EventArgs e)
        {
            // Closes the program.
            Close();
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            // Unused.
        }

        private void rollButton_Click(object sender, EventArgs e)
        {
            // Plays the game
            rollDice();
        }

        private void guessChoiceBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Unused. User's choice is checked through the currently selected index.
        }

        private void rollDice()
        {
            // D6 is rolled twice and the resulting sum is calculated.
            d1 = rnd.Next(1, 7);
            d2 = rnd.Next(1, 7);
            roll = d1 + d2;
            // Tell the used if their choice was right.
            textBox.AppendText("Computer rolled: " + d1 + " + " + d2 + " = " + roll + ".\n");
            if ((guessChoiceBox.SelectedIndex + 2) == roll)
            {
                textBox.AppendText("You guessed right!\n");
            }
            else
            {
                textBox.AppendText("Your guess was " + (guessChoiceBox.SelectedIndex + 2) + ". You guessed wrong.\n");
            }
            textBox.AppendText("Pick a number and click Roll to play again.\n");
        }
    }
}
