﻿namespace DiceGuessGame_GUI
{
    partial class DiceGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.quitButton = new System.Windows.Forms.Button();
            this.textBox = new System.Windows.Forms.TextBox();
            this.rollButton = new System.Windows.Forms.Button();
            this.guessChoiceBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // quitButton
            // 
            this.quitButton.Location = new System.Drawing.Point(212, 184);
            this.quitButton.Name = "quitButton";
            this.quitButton.Size = new System.Drawing.Size(60, 23);
            this.quitButton.TabIndex = 0;
            this.quitButton.Text = "Quit";
            this.quitButton.UseVisualStyleBackColor = true;
            this.quitButton.Click += new System.EventHandler(this.quitButton_Click);
            // 
            // textBox
            // 
            this.textBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBox.Location = new System.Drawing.Point(12, 12);
            this.textBox.Multiline = true;
            this.textBox.Name = "textBox";
            this.textBox.ReadOnly = true;
            this.textBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox.Size = new System.Drawing.Size(260, 166);
            this.textBox.TabIndex = 1;
            this.textBox.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // rollButton
            // 
            this.rollButton.Location = new System.Drawing.Point(78, 184);
            this.rollButton.Name = "rollButton";
            this.rollButton.Size = new System.Drawing.Size(128, 23);
            this.rollButton.TabIndex = 2;
            this.rollButton.Text = "Roll";
            this.rollButton.UseVisualStyleBackColor = true;
            this.rollButton.Click += new System.EventHandler(this.rollButton_Click);
            // 
            // guessChoiceBox
            // 
            this.guessChoiceBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.guessChoiceBox.FormattingEnabled = true;
            this.guessChoiceBox.Items.AddRange(new object[] {
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.guessChoiceBox.Location = new System.Drawing.Point(12, 186);
            this.guessChoiceBox.MaxDropDownItems = 12;
            this.guessChoiceBox.Name = "guessChoiceBox";
            this.guessChoiceBox.Size = new System.Drawing.Size(60, 21);
            this.guessChoiceBox.TabIndex = 4;
            this.guessChoiceBox.SelectedIndexChanged += new System.EventHandler(this.guessChoiceBox_SelectedIndexChanged);
            // 
            // DiceGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 219);
            this.Controls.Add(this.guessChoiceBox);
            this.Controls.Add(this.rollButton);
            this.Controls.Add(this.textBox);
            this.Controls.Add(this.quitButton);
            this.Name = "DiceGame";
            this.Text = "DiceGuessGame";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button quitButton;
        private System.Windows.Forms.TextBox textBox;
        private System.Windows.Forms.Button rollButton;
        private System.Windows.Forms.ComboBox guessChoiceBox;
    }
}

